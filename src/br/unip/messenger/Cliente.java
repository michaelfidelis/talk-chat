package br.unip.messenger;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Cliente extends JFrame {

	private static final long serialVersionUID = 1L;
	private static int PORTA = 9005;
	private BufferedReader in;
	private PrintWriter out;
	private JTextField textField;
	private JTextArea messageArea;
	private String usuario;

	public Cliente() {
		this.setTitle("Cliente de Mensagens |Talk");
		this.setSize(300, 350);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);

		textField = new JTextField();
		messageArea = new JTextArea();
		textField.setEditable(false);
		messageArea.setEditable(false);

		this.add(textField, BorderLayout.SOUTH);
		this.add(new JScrollPane(messageArea), BorderLayout.CENTER);

		textField.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				out.println(textField.getText());
				textField.setText("");
			}
		});
	}

	private String solicitarEnderecoServidor() {
		return JOptionPane.showInputDialog(null, "Digite o IP do Servidor:",
				"Bem vindo!", JOptionPane.QUESTION_MESSAGE);
	}

	private String solicitarApelidoUsuario() {
		return JOptionPane.showInputDialog(null, "Digite um apelido:",
				"Apelido", JOptionPane.PLAIN_MESSAGE);
	}

	@SuppressWarnings("resource")
	private void run() throws IOException {

		String serverAddress = solicitarEnderecoServidor();
		Socket socket = new Socket(serverAddress, PORTA);
		in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		out = new PrintWriter(socket.getOutputStream(), true);

		while (true) {
			String line = "";
			
			try {
				line = in.readLine();
			} catch (SocketException e) {
				JOptionPane.showMessageDialog(null,
						usuario + ", a conex�o com o servidor foi encerrada!", "Servidor",
						JOptionPane.ERROR_MESSAGE);
				this.textField.setEditable(false);
			}
			Mensagem mensagem = new Mensagem(line);

			switch (mensagem.getCabecalho()) {
			case NOVO_USUARIO:
				this.usuario = solicitarApelidoUsuario();
				out.println(usuario);
				break;
			case USUARIO_ACEITO:
				textField.setEditable(true);
				this.setTitle(usuario + " |Talk");
				break;
			case MENSAGEM:
				messageArea.append(mensagem.getFormatada());
				break;
			default:
				break;
			}
		}
	}

	public static void main(String[] args) throws Exception {
		Cliente cliente = new Cliente();
		cliente.setVisible(true);
		cliente.run();
	}
}