package br.unip.messenger;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.ServerSocket;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;

public class Servidor extends JFrame {

	private static final long serialVersionUID = 1L;
	private static final int PORTA = 9005;
	private DefaultListModel<String> listModel;
	private JList<String> usuariosConectados;
	private JButton atualizaUsuarios;

	public Servidor(int porta) {
		setTitle("Servidor rodando...");
		setSize(300, 350);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setLayout(new BorderLayout(0, 15));
		listModel = new DefaultListModel<String>();
		usuariosConectados = new JList<String>(listModel);
		usuariosConectados.setLayoutOrientation(JList.VERTICAL);

		atualizaUsuarios = new JButton("Atualizar usu�rios");
		atualizaUsuarios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateUsuariosConectados();
			}
		});

		add(new JLabel("Usu�rios conectados: "), BorderLayout.NORTH);
		add(new JScrollPane(usuariosConectados), BorderLayout.CENTER);
		add(atualizaUsuarios, BorderLayout.SOUTH);
	}

	public static void main(String[] args) throws Exception {
		Servidor servidor = new Servidor(PORTA);
		servidor.setVisible(true);
		servidor.iniciar();

	}

	public Servidor iniciar() throws IOException {
		ServerSocket listener = new ServerSocket(PORTA);
		try {
			while (true) {
				new Orquestrador(listener.accept()).start();
				updateUsuariosConectados();
			}
		} finally {
			listener.close();
			updateUsuariosConectados();
		}
	}

	public void updateUsuariosConectados() {
		listModel.clear();
		for (String usuario : Orquestrador.getUsuarios()) {
			listModel.addElement(usuario);
		}
	}
}