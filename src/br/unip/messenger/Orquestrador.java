package br.unip.messenger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashSet;

public class Orquestrador extends Thread {
	private static HashSet<String> usuarios = new HashSet<String>();
	private static HashSet<PrintWriter> writers = new HashSet<PrintWriter>();

	private String usuario;
	private Socket socket;
	private BufferedReader in;
	private PrintWriter out;

	public Orquestrador(Socket socket) {
		this.socket = socket;
	}

	public void run() {
		try {
			in = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			out = new PrintWriter(socket.getOutputStream(), true);

			while (true) {
				out.println(new Mensagem(Cabecalho.NOVO_USUARIO, usuario));
				usuario = in.readLine();
				if (usuario == null) {
					return;
				}
				synchronized (usuarios) {
					if (!usuarios.contains(usuario)) {
						usuarios.add(usuario);
						break;
					}
				}
			}

			out.println(new Mensagem(Cabecalho.USUARIO_ACEITO, usuario));
			writers.add(out);

			while (true) {
				String input = in.readLine();
				if (input == null) {
					return;
				}
				for (PrintWriter writer : writers) {
					writer.println(new Mensagem(Cabecalho.MENSAGEM, usuario,
							input));
				}
			}
		} catch (IOException e) {
			System.out.println(e);
		} finally {

			if (usuario != null) {
				usuarios.remove(usuario);
			}
			if (out != null) {
				writers.remove(out);
			}
			try {
				socket.close();
			} catch (IOException e) {
			}
		}
	}

	public static HashSet<String> getUsuarios() {
		return usuarios;
	}
}