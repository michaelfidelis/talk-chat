package br.unip.messenger;

public class Mensagem {
	private static final String SEPARADOR = "<#!>";
	private Cabecalho cabecalho;
	private String usuario;
	private String texto;

	public Mensagem(Cabecalho cabecalho, String usuario) {
		this.cabecalho = cabecalho;
		this.usuario = usuario;
		System.out.println("[Usu�rio] "+ usuario);
	}

	public Mensagem(Cabecalho cabecalho, String usuario, String texto) {
		this.cabecalho = cabecalho;
		this.usuario = usuario;
		this.texto = texto;
		System.out.println("[Usu�rio] "+ usuario);
	}

	public Mensagem(String texto) {
		decodifica(texto);
	}

	public Cabecalho getCabecalho() {
		return cabecalho;
	}

	public String getUsuario() {
		return usuario;
	}

	public String getTexto() {
		return texto;
	}

	public String getFormatada() {
		System.out.println("[Mensagem] "+ this.usuario +": "+ this.texto);
		return String.format("%s: %s %n", this.usuario, this.texto);
	}

	public String getCodificada() {
		if (usuario == null) {
			return cabecalho.name();
		}
		if (texto == null) {
			return cabecalho + SEPARADOR + usuario;
		} else {
			return cabecalho + SEPARADOR + usuario + SEPARADOR + texto;
		}
	}

	private void decodifica(String texto) {
		String[] msg = texto.split(SEPARADOR);
		System.out.println("[Cabe�alho] "+ msg[0]);
		
		this.cabecalho = Cabecalho.valueOf(msg[0]);

		if (cabecalho == Cabecalho.USUARIO_ACEITO) {
			this.usuario = msg[1];

		}
		if (cabecalho == Cabecalho.MENSAGEM) {
			this.usuario = msg[1];
			this.texto = msg[2];
		}
	}

	@Override
	public String toString() {
		return this.getCodificada();
	}
}
